﻿using System;
using SDL2;

namespace NicroWare.Lib.SdlEngine
{
    public class SDLFont : SDLWrapper
    {
        public SDLFont(IntPtr ptr)
            :base(ptr)
        {
            
        }

        public static SDLFont LoadFont(string path, int size)
        {
            return new SDLFont(SDL_ttf.TTF_OpenFont(path, size));
        }

        #region implemented abstract members of SDLWrapper

        protected override void Disposer()
        {
            //TODO: find out why this is not working as it should
            //SDL_ttf.TTF_CloseFont(base.BasePointer);
        }

        #endregion
    }
}

