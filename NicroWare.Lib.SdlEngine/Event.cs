﻿using System;
using SDL2;
using System.Collections.Generic;

namespace NicroWare.Lib.SdlEngine.Events
{
    public enum KeyState
    {
        KeyUp,
        KeyDown
    }

    public class KeyEventArgs : EventArgs
    {
        public Keys KeyChar { get; set; }
        public KeyState State { get; set; }

    }
    public delegate void KeyEvent(object sender, KeyEventArgs e);
    public delegate void EventUpdater();

    public class Event
    {
        public static bool Exit { get; set; }
        public static event KeyEvent KeyDown;
        public static event KeyEvent KeyUp;
        public static event EventUpdater Updater;


        public Event()
        {
        }

        public static void DoEvents()
        {
            SDL.SDL_Event e;
            while (SDL.SDL_PollEvent(out e) != 0)
            {
                if (e.type == SDL.SDL_EventType.SDL_KEYDOWN)
                {
                    Keyboard.AddKey((Keys)e.button.button);
                    if (KeyDown != null)
                        KeyDown(null, new KeyEventArgs() { KeyChar = (Keys)e.button.button, State = KeyState.KeyDown });
                }
                else if (e.type == SDL.SDL_EventType.SDL_KEYUP)
                {
                    Keyboard.RemoveKey((Keys)e.button.button);
                    if (KeyDown != null)
                        KeyDown(null, new KeyEventArgs() { KeyChar = (Keys)e.button.button, State = KeyState.KeyUp });
                }
                else if (e.type == SDL.SDL_EventType.SDL_QUIT)
                    Exit = true;
                else if (e.type == SDL.SDL_EventType.SDL_MOUSEBUTTONDOWN)
                {
                    Mouse.AddKey((MouseButtons)e.button.button);
                    int x, y;
                    SDL.SDL_GetMouseState(out x, out y);
                    Mouse.SetMousePos(x, y);
                }
                else if (e.type == SDL.SDL_EventType.SDL_MOUSEMOTION)
                {
                    int x, y;
                    SDL.SDL_GetMouseState(out x, out y);
                    Mouse.SetMousePos(x, y);
                }
                else if (e.type == SDL.SDL_EventType.SDL_MOUSEBUTTONUP)
                {
                    Mouse.RemoveKey((MouseButtons)e.button.button);
                    int x, y;
                    SDL.SDL_GetMouseState(out x, out y);
                    Mouse.SetMousePos(x, y);
                }
            }
            if (Updater != null)
                Updater();
        }
    }

    public enum Keys : int
    {
        A = 4,
        B = 5,
        C = 6,
        D = 7,
        E = 8,
        F = 9,
        G = 10,
        H = 11,
        I = 12,
        J = 13,
        K = 14,
        L = 15,
        M = 16,
        N = 17,
        O = 18,
        P = 19,
        Q = 20,
        R = 21,
        S = 22,
        T = 23,
        U = 24,
        V = 25,
        W = 26,
        X = 27,
        Y = 28,
        Z = 29,
        ESC = 41,
        RIGHT = 79,
        LEFT = 80,
        DOWN = 81,
        UP = 82,
    }

    public enum MouseButtons : int
    {
        Left = 1,
        Middle = 2,
        Right = 3
    }

    public class Keyboard
    {
        static List<Keys> keyDown = new List<Keys>();

        public Keyboard()
        {
            
        }

        public static void AddKey(Keys key)
        {
            if (!keyDown.Contains(key))
                keyDown.Add(key);
        }

        public static void RemoveKey(Keys key)
        {
            keyDown.Remove(key);
        }

        public static KeyboardState GetState()
        {
            return new KeyboardState(keyDown.ToArray());
        }
    }

    public struct KeyboardState
    {
        public Keys[] Keys { get; private set; }

        public KeyboardState(Keys[] keys)
        {
            this.Keys = keys;
        }

        public bool IsKeyDown(Keys key)
        {
            if (Keys == null)
                return false;
            foreach(Keys keyC in Keys)
            {
                if (keyC == key)
                    return true;
            }
            return false;
        }
    }

    public class Mouse
    {
        static int x, y;
        static List<MouseButtons> keyDown = new List<MouseButtons>();
        static MouseState? currentState = null;

        public static void SetMousePos(int x, int y)
        {
            currentState = null;
            Mouse.x = x;
            Mouse.y = y;
        }

        public static void AddKey(MouseButtons key)
        {
            currentState = null;
            if (!keyDown.Contains(key))
                keyDown.Add(key);
        }

        public static void RemoveKey(MouseButtons key)
        {
            currentState = null;
            keyDown.Remove(key);
        }

        public static MouseState GetState()
        {
            if (currentState == null)
                currentState = new MouseState(new SDLPoint(x, y), keyDown.ToArray());
            return currentState.Value;
        }
    }

    public struct MouseState
    {
        public SDLPoint Location { get; private set; }
        public MouseButtons[] Buttons { get; private set; }

        public MouseState(SDLPoint location, MouseButtons[] buttons)
        {
            this.Location = location;
            this.Buttons = buttons;
        }

        public bool IsKeyUp(MouseButtons key)
        {
            return !IsKeyDown(key);
        }

        public bool IsKeyDown(MouseButtons key)
        {
            if (Buttons == null)
                return false;
            foreach(MouseButtons keyC in Buttons)
            {
                if (keyC == key)
                    return true;
            }
            return false;
        }
    }
}

