﻿using System;
using SDL2;

namespace NicroWare.Lib.SdlEngine
{
    public struct SDLColor
    {
        public byte R { get; set; }
        public byte G { get; set; }
        public byte B { get; set; }
        public byte A { get; set; }

        public SDLColor(byte red, byte green, byte blue)
            :this()
        {
            this.A = 255;
            this.R = red;
            this.G = green;
            this.B = blue;
        }

        public SDLColor(byte alpha, byte red, byte green, byte blue)
            :this(red, green, blue)
        {
            this.A = alpha;
        }

        public static implicit operator SDL.SDL_Color(SDLColor color)
        {
            return new SDL.SDL_Color() { r = color.R, g = color.G, b = color.B, a = color.A };
        }

        public static implicit operator SDLColor(SDL.SDL_Color color)
        {
            return new SDLColor(color.a, color.r, color.g, color.b);
        }

        public static implicit operator System.Drawing.Color(SDLColor color)
        {
            return System.Drawing.Color.FromArgb(color.A, color.R, color.G, color.B);
        }

        public static implicit operator SDLColor(System.Drawing.Color color)
        {
            return new SDLColor(color.A, color.R, color.G, color.B);
        }

        public static readonly SDLColor White = new SDLColor(255,255,255);
        public static readonly SDLColor Black = new SDLColor(0,0,0);
        public static readonly SDLColor Red = new SDLColor(255,0,0);
        public static readonly SDLColor Green = new SDLColor(0,255,0);
        public static readonly SDLColor Blue = new SDLColor(0,0,255);

    }
}

