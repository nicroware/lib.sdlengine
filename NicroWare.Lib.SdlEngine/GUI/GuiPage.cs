﻿using System;
using System.Collections.Generic;

namespace NicroWare.Lib.SdlEngine
{
    public class GuiPage
    {
        public List<GuiElement> Elements { get; private set; }// = new List<GuiElement>();
        //SDLRenderer renderer;
        PageRenderer pageRenderer;
        public SDLPoint Location { get; set; }

        public GuiPage(SDLRenderer renderer)
        {
            //this.renderer = renderer;
            Elements = new List<GuiElement>();
            pageRenderer = new PageRenderer(renderer, this);
        }

        public void Initialize(SDLRenderer renderer)
        {
            Elements.ForEach(x => { x.PageRenderer = pageRenderer; x.Initialize(renderer); });
        }

        public void Update(GameTime gameTime)
        {
            Elements.ForEach(x => x.Update(gameTime));
        }



        public void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            Elements.ForEach(x => x.Draw(x.Renderer, gameTime));
        }
    }
}

