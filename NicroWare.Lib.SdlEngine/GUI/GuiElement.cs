﻿using NicroWare.Lib.SdlEngine.Events;
using System;

namespace NicroWare.Lib.SdlEngine
{
    public abstract class GuiElement
    {
        public virtual SDLPoint Location { get; set; }
        public virtual SDLPoint Size { get; set; }
        public virtual string Text { get; set; }
        public ControlRenderer Renderer { get; set; }
        public PageRenderer PageRenderer { get; set; }
        protected MouseState current;
        protected MouseState last;

        public GuiElement()
        {
            Location = new SDLPoint(0, 0);
            Size = new SDLPoint(100, 100);
            Text = "";
        }

        protected void PullEvents()
        {
            last = current;
            current = Mouse.GetState();
        }

        public virtual void Initialize(SDLRenderer renderer)
        {
            Renderer = new ControlRenderer(renderer, this);
        }

        public virtual void Update(GameTime gameTime)
        {
            
        }

        public virtual void Draw(Renderer renderer, GameTime gameTime)
        {
            
        }
    }
}

