﻿using System;

namespace NicroWare.Lib.SdlEngine
{
    public enum Align
    {
        Left,
        Center,
        Right,
    }
    
    public class GuiTextElement : GuiElement
    {
        public virtual SDLColor TextColor { get; set; }
        public virtual bool DynamicTextSize { get; set; }
        public virtual SDLFont Font { get; set; }
        public virtual int FontSize { get; set; }
        public virtual Align TextAlign { get; set; }

        public static SDLFont GlobalFont { get; set; }

        public GuiTextElement()
        {
            DynamicTextSize = false;
            TextColor = SDLColor.White;
            Font = GlobalFont;
            FontSize = 12;
            TextAlign = Align.Left;
        }

        public virtual int CalculateWidth(string text)
        {
            return (int)(FontSize * 0.6 * text.Length);
        }

        public virtual int CalculateAlign(string text)
        {
            int width = CalculateWidth(text);
            switch (TextAlign)
            {
                default:
                    return 0;
                case Align.Right:
                    return Size.X - width;
                case Align.Center:
                    return (int)(Size.X / 2.0 - width / 2.0);
            }
        }
    }
}

