﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;

namespace NicroWare.Lib.SdlEngine
{
    public class Input
    {
        //static List<InputEvent> allEvents = new InputEvent();
        static Queue<InputEvent> allEvents = new Queue<InputEvent>();
        //static Task mainFetcher;
        public static bool Initialized = false;

        public static void Initialize()
        {
            Task.Run(new Action(FetchEvents));
        }

        public static void FetchEvents()
        {
            try
            {
                Initialized = true;
                FileStream fs = File.OpenRead("/dev/input/by-id/usb-Microchip_Technology_Inc._AR1100_HID-MOUSE-event-mouse");
                byte[] buffer = new byte[16];
                while (true)
                {
                    for (int c = 0; c < buffer.Length;)
                        c += fs.Read(buffer, 0, buffer.Length);
                    InputEvent ie = InputEvent.FromShortByteArray(buffer);
                    //allEvents.Add(ie);
                    allEvents.Enqueue(ie);
                }
            }
            catch
            {
                Initialized = false;
                Console.WriteLine("Cant read from touch input");
                SDL2.SDL.SDL_ShowCursor(1);
            }
        }

        public static InputEvent? GetEvent()
        {
            if (allEvents.Count == 0)
                return null;
            else
                return allEvents.Dequeue();
        }
    }

    public struct TimeVal
    {
        public long tv_sec;
        public long tv_usec;
    }

    public struct InputEvent
    {
        public TimeVal time;
        public ushort type;
        public ushort code;
        public uint value;

        public static InputEvent FromByteArray(byte[] input)
        {
            InputEvent ie = new InputEvent();
            ie.time = new TimeVal();
            ie.time.tv_sec = BitConverter.ToInt64(input, 0);
            ie.time.tv_usec = BitConverter.ToInt64(input, 8);
            ie.type = BitConverter.ToUInt16(input, 16);
            ie.code = BitConverter.ToUInt16(input, 18);
            ie.value = BitConverter.ToUInt32(input, 20);
            return ie;
        }

        public static InputEvent FromShortByteArray(byte[] input)
        {
            InputEvent ie = new InputEvent();
            ie.time = new TimeVal();
            ie.time.tv_sec = BitConverter.ToInt32(input, 0);
            ie.time.tv_usec = BitConverter.ToInt32(input, 4);
            ie.type = BitConverter.ToUInt16(input, 8);
            ie.code = BitConverter.ToUInt16(input, 10);
            ie.value = BitConverter.ToUInt32(input, 12);
            return ie;
        }

        const int Size = 24;
    }
}

