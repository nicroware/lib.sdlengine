using System;
using SDL2;

namespace NicroWare.Lib.SdlEngine
{
    public struct SDLPoint
	{
        public int X { get; set; }
        public int Y { get; set; }

        public SDLPoint(int x, int y)
            : this()
        {
            this.X = x;
            this.Y = y;
        }

        public static SDLPoint operator+(SDLPoint a, SDLPoint b)
        {
            return new SDLPoint(a.X + b.X, a.Y + b.Y);
        }

        public static SDLPoint operator-(SDLPoint a, SDLPoint b)
        {
            return new SDLPoint(a.X - b.X, a.Y - b.Y);
        }

        public static implicit operator SDLPoint(SDL.SDL_Point point)
        {
            return new SDLPoint(point.x, point.y);
        }

        public static implicit operator SDL.SDL_Point(SDLPoint point)
        {
            return new SDL.SDL_Point() { x = point.X, y = point.Y };
        }
	}

}

