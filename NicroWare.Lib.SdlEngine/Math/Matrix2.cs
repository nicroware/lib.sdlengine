﻿using System;

namespace NicroWare.Lib.SdlEngine
{
    public struct Vector2
    {
        public double X { get; set; }
        public double Y { get; set; }

        public static Vector2 operator +(Vector2 a, Vector2 b)
        {
            return new Vector2() { X = a.X + b.X, Y = a.Y + b.Y };
        }
    }

    public struct Matrix2
    {
        public double[] Numbers { get; private set; }

        public Matrix2(double[] startMatix)
        {
            if (startMatix.Length != 9)
                throw new Exception("Wrong size, Array has to be 9 big");
            Numbers = startMatix;
        }

        public static Matrix2 CreateEmpty()
        {
            return new Matrix2() { Numbers = new double[9] };
        }

        public static Matrix2 CreateTranslation(double x, double y)
        {
            Matrix2 returnMatrix = Matrix2.CreateEmpty();

            returnMatrix.Numbers[0] = 1;
            returnMatrix.Numbers[1] = 0;
            returnMatrix.Numbers[2] = x;

            returnMatrix.Numbers[3] = 0;
            returnMatrix.Numbers[4] = 1;
            returnMatrix.Numbers[5] = y;

            returnMatrix.Numbers[6] = 0;
            returnMatrix.Numbers[7] = 0;
            returnMatrix.Numbers[8] = 1;

            return returnMatrix;
        }

        public static Matrix2 CreateRotation(double rad)
        {
            Matrix2 returnMatrix = Matrix2.CreateEmpty();

            returnMatrix.Numbers[0] = Math.Cos(rad);
            returnMatrix.Numbers[1] = -Math.Sin(rad);
            returnMatrix.Numbers[2] = 0;

            returnMatrix.Numbers[3] = Math.Sin(rad);
            returnMatrix.Numbers[4] = Math.Cos(rad);
            returnMatrix.Numbers[5] = 0;

            returnMatrix.Numbers[6] = 0;
            returnMatrix.Numbers[7] = 0;
            returnMatrix.Numbers[8] = 1;
            return returnMatrix;
        }

        public static Matrix2 operator *(Matrix2 a, Matrix2 b)
        {
            Matrix2 newMatrix = Matrix2.CreateEmpty();
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 3; x++)
                {
                    double tempNum = 0;
                    for (int i = 0; i < 3; i++)
                    {
                        tempNum += a.Numbers[y * 3 + i] * b.Numbers[x + i * 3];
                    }
                    newMatrix.Numbers[x + y * 3] = tempNum;
                }
            }
            return newMatrix;
        }

        public static Vector2 operator *(Matrix2 a, Vector2 b)
        {
            return new Vector2() { X = a.Numbers[0] * b.X + a.Numbers[1] * b.Y + a.Numbers[2] * 1, Y = a.Numbers[3] * b.X + a.Numbers[4] * b.Y + a.Numbers[5] * 1 };
        }
    }
}

