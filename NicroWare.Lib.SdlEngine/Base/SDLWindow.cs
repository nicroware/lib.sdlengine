﻿using System;
using SDL2;
using System.Diagnostics;

namespace NicroWare.Lib.SdlEngine
{
    public class SDLWindow : SDLWrapper
    {

        protected SDLWindow()
            : base()
        {
        }

        public SDLWindow(IntPtr basePointer)
            :base (basePointer)
        {

        }

        public SDLWindow(SDLWindow baseWindow)
            :this(baseWindow.BasePointer)
        {

        }


        public static SDLWindow Create(string title, int x, int y, int width, int height)
        {
            return new SDLWindow(SDL.SDL_CreateWindow(title, x, y, width, height, SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN));
        }

        protected static IntPtr CreatePointer(string title, int x, int y, int width, int height)
        {
            return SDL.SDL_CreateWindow(title, x, y, width, height, SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN);
        }

        #region IDisposable implementation

        protected override void Disposer()
        {
            SDL.SDL_DestroyWindow(BasePointer);
            BasePointer = IntPtr.Zero;
        }

        #endregion
    }
}

