﻿using System;
using SDL2;

namespace NicroWare.Lib.SdlEngine
{
    public abstract class SDLWrapper : IDisposable
    {
        public IntPtr BasePointer {get; set;}
        protected bool disposed = false;

        protected SDLWrapper()
        {
            if (!PointerInitializer())
                throw new ArgumentNullException("Base Pointer is null");
        }

        protected virtual bool PointerInitializer()
        {
            return false;
        }

        public SDLWrapper(IntPtr basePointer)
        {
            if (basePointer == IntPtr.Zero)
                throw new NullReferenceException("Null Pointer exception: " + SDL.SDL_GetError());
            this.BasePointer = basePointer;
        }

        ~SDLWrapper()
        {
            if (!disposed)
                Dispose();
        }

        #region IDisposable implementation

        public void Dispose()
        {
            if (!disposed)
            {
                Disposer();
                BasePointer = IntPtr.Zero;
                disposed = true;
            }
        }

        protected abstract void Disposer();

        #endregion
    }
}

