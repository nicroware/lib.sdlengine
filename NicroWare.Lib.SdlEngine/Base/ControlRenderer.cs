﻿using System;

namespace NicroWare.Lib.SdlEngine
{
    public enum RendererFlip : int
    {
        None = 0,
        Horisontal = 1,
        Vertical = 2,
        Both = 3,
    }

    public abstract class Renderer
    {
        protected SDLRenderer baseRenderer;

        public Renderer(SDLRenderer baseRenderer)
        {
            this.baseRenderer = baseRenderer;
        }

        public virtual void Draw(SDLTexture texture, SDLRectangle rectangle)
        {
            baseRenderer.DrawTexture(texture, rectangle);
        }

        public virtual void Draw(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip)
        {
            baseRenderer.DrawTexture(texture, sourceRect, destRect, angle, centerPoint, flip);
        }

        public virtual void Draw(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip, SDLColor color)
        {
            baseRenderer.DrawTexture(texture, sourceRect, destRect, angle, centerPoint, flip, color);
        }

        public virtual void DrawText(string text, SDLFont font, SDLRectangle rect, SDLColor color)
        {
            baseRenderer.DrawText(text, font, rect, color);
        }
    }

    public class PageRenderer : Renderer
    {
        public SDLPoint Location { get { return parrent.Location; } }
        GuiPage parrent;

        public PageRenderer(SDLRenderer renderer, GuiPage element) 
            : base(renderer)
        {
            parrent = element;
        }

        private SDLRectangle ConvertRectangle(SDLRectangle rectangle)
        {
            return new SDLRectangle(rectangle.X + Location.X, rectangle.Y + Location.Y, rectangle.Width, rectangle.Height);
        }

        public override void Draw(SDLTexture texture, SDLRectangle rectangle)
        {
            base.Draw(texture, ConvertRectangle(rectangle));
        }

        public override void Draw(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip)
        {
            base.Draw(texture, sourceRect, ConvertRectangle(destRect), angle, centerPoint, flip);
        }

        public override void Draw(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip, SDLColor color)
        {
            base.Draw(texture, sourceRect, ConvertRectangle(destRect), angle, centerPoint, flip, color);
        }

        public override void DrawText(string text, SDLFont font, SDLRectangle rect, SDLColor color)
        {
            base.DrawText(text, font, ConvertRectangle(rect), color);
        }

    }

    public class ControlRenderer : Renderer
    {
        public SDLPoint Location { get { return parrent.Location; } }
        GuiElement parrent;

        public ControlRenderer(SDLRenderer renderer, GuiElement element) 
            : base(renderer)
        {
            parrent = element;
        }

        private SDLRectangle ConvertRectangle(SDLRectangle rectangle)
        {
            return new SDLRectangle(rectangle.X + Location.X, rectangle.Y + Location.Y, rectangle.Width, rectangle.Height);
        }

        public override void Draw(SDLTexture texture, SDLRectangle rectangle)
        {
            parrent.PageRenderer.Draw(texture, ConvertRectangle(rectangle));
        }

        public override void Draw(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip)
        {
            parrent.PageRenderer.Draw(texture, sourceRect, ConvertRectangle(destRect), angle, centerPoint, flip);
        }

        public override void Draw(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip, SDLColor color)
        {
            parrent.PageRenderer.Draw(texture, sourceRect, ConvertRectangle(destRect), angle, centerPoint, flip, color);
        }

        public override void DrawText(string text, SDLFont font, SDLRectangle rect, SDLColor color)
        {
            parrent.PageRenderer.DrawText(text, font, ConvertRectangle(rect), color);
        }
    }
}

