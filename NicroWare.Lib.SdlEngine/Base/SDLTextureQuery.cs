﻿using System;

namespace NicroWare.Lib.SdlEngine
{
    public struct SDLTextureQuery
    {
        public uint format;
        public int access;
        public int width;
        public int height;
    }
}

