﻿using System;
using SDL2;

namespace NicroWare.Lib.SdlEngine
{
    public class SDLSurface : SDLWrapper
    {
        public SDLSurface(IntPtr basePointer)
            :base(basePointer)
        {
        }

        public static SDLSurface LoadBitmap(string path)
        {
            return new SDLSurface(SDL.SDL_LoadBMP(path));
        }

        public static SDLSurface FromPointer(IntPtr ptr)
        {
            return new SDLSurface(ptr);
        }

        #region implemented abstract members of SDLWrapper

        protected override void Disposer()
        {
            SDL.SDL_FreeSurface(BasePointer);
        }

        #endregion
    }
}

