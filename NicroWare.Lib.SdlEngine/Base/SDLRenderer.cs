﻿using System;
using SDL2;

namespace NicroWare.Lib.SdlEngine
{
    public class SDLRenderer : SDLWrapper
    {
        public SDLColor DrawColor { get; set; }
        public SDLRenderer(IntPtr basePointer)
            :base(basePointer)
        {
        }

        public static SDLRenderer Create(SDLWindow sdlWindow)
        {
            return new SDLRenderer(SDL.SDL_CreateRenderer(sdlWindow.BasePointer, -1, (uint)SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED));
        }

        public void Clear()
        {
            SDL.SDL_RenderClear(BasePointer);
        }

        public void Present()
        {
            SDL.SDL_RenderPresent(BasePointer);
        }

        public void Copy(SDLTexture texture, ref SDLRectangle rect)
        {
            SDL.SDL_Rect temp = rect;
            int error = SDL.SDL_RenderCopy(BasePointer, texture.BasePointer, IntPtr.Zero, ref temp);
            if (error != 0)
                Console.WriteLine(SDL.SDL_GetError());
            rect = temp;
        }

        public void Copy(SDLTexture texture, ref SDLRectangle sourceRect, ref SDLRectangle destRect, double angle, ref SDLPoint centerPoint, RendererFlip flip)
        {
            SDL.SDL_Rect src = sourceRect;
            SDL.SDL_Rect dst = destRect;
            SDL.SDL_Point p = centerPoint;
            SDL.SDL_RenderCopyEx(BasePointer, texture.BasePointer, ref src, ref dst, angle, ref  p, (SDL.SDL_RendererFlip)flip);
        }

        public void Copy(SDLTexture texture, ref SDLRectangle sourceRect, ref SDLRectangle destRect, double angle, ref SDLPoint centerPoint, RendererFlip flip, SDLColor color)
        {
            SDL.SDL_Rect src = sourceRect;
            SDL.SDL_Rect dst = destRect;
            SDL.SDL_Point p = centerPoint;
            SDL.SDL_SetTextureColorMod(texture.BasePointer, color.R, color.G, color.B);
            SDL.SDL_RenderCopyEx(BasePointer, texture.BasePointer, ref src, ref dst, angle, ref  p, (SDL.SDL_RendererFlip)flip);
        }

        public void DrawTexture(SDLTexture texture, SDLRectangle destination)
        {
            Copy(texture, ref destination);
        }

        public void DrawTexture(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip)
        {
            Copy(texture, ref sourceRect, ref destRect, angle / (Math.PI * 2) * 360, ref centerPoint, flip);
        }

        public void DrawTexture(SDLTexture texture, SDLRectangle sourceRect, SDLRectangle destRect, double angle, SDLPoint centerPoint, RendererFlip flip, SDLColor color)
        {
            Copy(texture, ref sourceRect, ref destRect, angle / (Math.PI * 2) * 360, ref centerPoint, flip, color);
        }

        public void DrawText(string text, SDLFont font, SDLRectangle destination, SDLColor color)
        {
            if (text.Length > 0)
            {
                SDLSurface temp = SDLSurface.FromPointer(SDL_ttf.TTF_RenderText_Solid(font.BasePointer, text, color));
                SDLTexture temp2 = SDLTexture.CreateFrom(this, temp);
                DrawTexture(temp2, destination);
                temp.Dispose();
                temp2.Dispose();
            }
        }

        public void SetColor(SDLColor color)
        {
            DrawColor = color;
            SDL.SDL_SetRenderDrawColor(BasePointer, color.R, color.G, color.B, color.A);
        }

        public void DrawPoint(int x, int y)
        {
            SDL.SDL_RenderDrawPoint(BasePointer, x, y);
        }

        public void DrawPoint(int x, int y, SDLColor color)
        {
            SDLColor pre = DrawColor;
            SetColor(color);
            DrawPoint(x, y);
            SetColor(pre);
        }

        #region IDisposable implementation

        protected override void Disposer()
        {
            SDL.SDL_DestroyRenderer(BasePointer);
        }

        #endregion
    }
}

