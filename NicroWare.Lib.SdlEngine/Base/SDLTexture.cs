﻿using System;
using SDL2;

namespace NicroWare.Lib.SdlEngine
{
    public enum PixelType : byte
    {
        Unknown = 0,
        Index1 = 1,
        Index4 = 2,
        Index8 = 3,
        Packed8 = 4,
        Packed16 = 5,
        Packed32 = 6,
        ArrayU8 = 7,
        ArrayU16 = 8,
        ArrayU32 = 9,
        ArrayF16 = 10,
        ArrayF32 = 11
    }

    public enum BitMapOrder : byte
    {
        None = 0,
        Order4321 = 1,
        Order1234 = 2
    }

    public enum PackedOrder : byte
    {
        None = 0,
        XRGB = 1,
        RGBX = 2,
        ARGB = 3,
        RGBA = 4,
        XBGR = 5,
        BGRX = 6,
        ABGR = 7,
        BGRA = 8
    }

    public enum ArrayOrder : byte
    {
        None = 0,
        RGB = 1,
        RGBA = 2,
        ARGB = 3,
        BGR = 4,
        BGRA = 5,
        ABGR = 6
    }

    public enum PackedLayout : byte
    {
        None = 0,
        L332 = 1,
        L4444 = 2,
        L1555 = 3,
        L565 = 4,
        L8888 = 5,
        L2101010 = 6,
        L1010102 = 7,
    }

    /*
    SDL.SDL_PIXELFORMAT_ABGR1555;
    SDL.SDL_PIXELFORMAT_ABGR4444;
    SDL.SDL_PIXELFORMAT_ABGR8888;
    SDL.SDL_PIXELFORMAT_ARGB1555;
    SDL.SDL_PIXELFORMAT_ARGB2101010;
    SDL.SDL_PIXELFORMAT_ARGB4444;
    SDL.SDL_PIXELFORMAT_ARGB8888;
    SDL.SDL_PIXELFORMAT_BGR24;
    SDL.SDL_PIXELFORMAT_BGR555;
    SDL.SDL_PIXELFORMAT_BGR565;
    SDL.SDL_PIXELFORMAT_BGR888;
    SDL.SDL_PIXELFORMAT_BGRA4444;
    SDL.SDL_PIXELFORMAT_BGRA5551;
    SDL.SDL_PIXELFORMAT_BGRA8888;
    SDL.SDL_PIXELFORMAT_BGRX8888;
    SDL.SDL_PIXELFORMAT_INDEX1LSB;
    SDL.SDL_PIXELFORMAT_INDEX1MSB;
    SDL.SDL_PIXELFORMAT_INDEX4LSB;
    SDL.SDL_PIXELFORMAT_INDEX4MSB;
    SDL.SDL_PIXELFORMAT_INDEX8;
    SDL.SDL_PIXELFORMAT_IYUV;
    SDL.SDL_PIXELFORMAT_RGB24;
    SDL.SDL_PIXELFORMAT_RGB332;
    SDL.SDL_PIXELFORMAT_RGB444;
    SDL.SDL_PIXELFORMAT_RGB555;
    SDL.SDL_PIXELFORMAT_RGB565;
    SDL.SDL_PIXELFORMAT_RGB888;
    SDL.SDL_PIXELFORMAT_RGBA4444;
    SDL.SDL_PIXELFORMAT_RGBA5551;
    SDL.SDL_PIXELFORMAT_RGBA8888;
    SDL.SDL_PIXELFORMAT_RGBX8888;
    SDL.SDL_PIXELFORMAT_UNKNOWN;
    SDL.SDL_PIXELFORMAT_UYVY;
    SDL.SDL_PIXELFORMAT_YUY2;
    SDL.SDL_PIXELFORMAT_YV12;
    SDL.SDL_PIXELFORMAT_YVYU;
    */
    public enum PixelFormat : uint
    {
        RGBA8888 = 0x16462004
    }

    public enum BlendMode : int
    {
        None = 0,
        Blend = 1,
        Add = 2,
        Mod = 4,
    }

    public class SDLTexture : SDLWrapper
    {

        public SDLTexture(IntPtr basePosition)
            :base(basePosition)
        {
            
        }

        public int Width { get; private set; }
        public int Height { get; private set; }

        SDL.SDL_BlendMode blendMode;

        public BlendMode BlendMode
        {
            get
            { 
                if (blendMode == default(SDL.SDL_BlendMode))
                    SDL.SDL_GetTextureBlendMode(BasePointer, out blendMode);
                return (BlendMode)blendMode; 
            }
            set
            { 
                blendMode = (SDL.SDL_BlendMode)value;
                SDL.SDL_SetTextureBlendMode(BasePointer, blendMode);
            }
        }

        public static SDLTexture CreateEmpty(SDLRenderer renderer, int width, int height)
        {
            return new SDLTexture(SDL.SDL_CreateTexture(renderer.BasePointer, (uint)SDL.SDL_PIXELFORMAT_RGB24, (int)SDL.SDL_TextureAccess.SDL_TEXTUREACCESS_STREAMING, width, height)) { Width = width, Height = height };
        }

        public static SDLTexture CreateEmpty(SDLRenderer renderer, int width, int height, uint SDLPixelFormat)
        {
            return new SDLTexture(SDL.SDL_CreateTexture(renderer.BasePointer, SDLPixelFormat, (int)SDL.SDL_TextureAccess.SDL_TEXTUREACCESS_STREAMING, width, height)) { Width = width, Height = height };
        }

        public static SDLTexture CreateEmpty(SDLRenderer renderer, int width, int height, PixelFormat format)
        {
            return new SDLTexture(SDL.SDL_CreateTexture(renderer.BasePointer, (uint)format, (int)SDL.SDL_TextureAccess.SDL_TEXTUREACCESS_STREAMING, width, height)) { Width = width, Height = height };
        }

        public static SDLTexture CreateFrom(SDLRenderer renderer, SDLSurface surface)
        {
            return new SDLTexture(SDL.SDL_CreateTextureFromSurface(renderer.BasePointer, surface.BasePointer)).Initialize(x => x.Query());
        }

        public IntPtr Lock()
        {
            IntPtr pixels;
            int pitch;
            SDL.SDL_LockTexture(BasePointer, IntPtr.Zero, out pixels, out pitch);
            return pixels;
        }

        public void UnLock()
        {
            SDL.SDL_UnlockTexture(BasePointer);
        }

        public SDLTextureQuery Query()
        {
            SDLTextureQuery query = new SDLTextureQuery();
            SDL.SDL_QueryTexture(BasePointer, out query.format, out query.access, out query.width, out query.height);
            this.Width = query.width;
            this.Height = query.height;
            return query;
        }

        #region implemented abstract members of SDLWrapper

        protected override void Disposer()
        {
            SDL.SDL_DestroyTexture(BasePointer);
        }

        #endregion

    }
}

