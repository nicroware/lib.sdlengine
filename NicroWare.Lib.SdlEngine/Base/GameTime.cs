﻿using System;
using System.Diagnostics;

namespace NicroWare.Lib.SdlEngine
{
    public class StartableGameTime : GameTime
    {
        public void Update()
        {
            lastUpdate = thisUpdate;
            thisUpdate = TotalElapsed;
        }
    }

    public class GameTime
    {
        protected TimeSpan lastUpdate;
        protected TimeSpan thisUpdate;

        public TimeSpan TotalElapsed 
        {
            get
            {
                return TimeSpan.FromMilliseconds(SDL2.SDL.SDL_GetTicks());
            }
        }
        public TimeSpan SinceLastUpdate
        {
            get
            { 
                return TotalElapsed - lastUpdate;
            }
        }

        public GameTime()
        {
            
        }
    }
}

