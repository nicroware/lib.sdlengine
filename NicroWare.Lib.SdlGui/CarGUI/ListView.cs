﻿using System;
using NicroWare.Lib.SdlEngine;
using System.Collections.Generic;
using NicroWare.Lib.SdlEngine.Events;

namespace NicroWare.Lib.SdlGui
{
    public class ListView : GuiTextElement
    {
        SDLTexture frame;
        SDLColor frameColor;
        public List<object> Items { get; private set; }
        public int Scroll { get; set; }
        int lastScroll = 0;
        int lastCtrlPos = 0;
        bool CapturedMouse;


        public ListView()
        {
            frameColor = new SDLColor(255, 255, 255);
            this.Items = new List<object>();
        }

        public override void Initialize(SDLRenderer renderer)
        {
            frame = new DrawSurface(Size.X, Size.Y).Initialize(surf => surf.CustomDraw((x,y) => 
            {
                if (x == 0 || y == 0 || y == Size.Y - 1 || x == Size.X - 1)
                {
                    surf[x,y] = frameColor;
                }
            })).MakeTexture(renderer);
            base.Initialize(renderer);
        }

        public override void Update(GameTime gameTime)
        {
            PullEvents();
            /*if (MouseData.Down
                && Global.SideScroll 
                && MouseData.LastPos.X > Location.X 
                && MouseData.LastPos.X < Location.X + Size.X 
                && MouseData.LastPos.Y > Location.Y 
                && MouseData.LastPos.Y < Location.Y + Size.Y)*/
            if (current.IsKeyDown(MouseButtons.Left) 
                && new SDLRectangle(Location, Size).Intersect(current.Location))
            {
                CapturedMouse = true;
                Global.SideScroll = false;
                lastScroll = Scroll;
                lastCtrlPos = MouseData.Pos.Y - Location.Y;
            }
            else if (MouseData.Down == false)
                CapturedMouse = false;
            if (CapturedMouse)
            {
                int ctrlPos = MouseData.Pos.Y - Location.Y;
                Scroll = (int)((lastCtrlPos - ctrlPos)) + lastScroll;
                //Console.WriteLine(scroll);
                if (Scroll < 0)
                    Scroll = 0;
                if (Items.Count * FontSize > Size.Y && Scroll > FontSize * (Items.Count + 1)  - Size.Y)
                    Scroll = FontSize * (Items.Count + 1) - Size.Y;
            }

        }

        public override void Draw(Renderer renderer, GameTime gameTime)
        {
            renderer.Draw(frame, new SDLRectangle(0, 0, Size.X, Size.Y));
            for (int i = (int)(Scroll / FontSize); i < Items.Count && (i) < ((Size.Y + Scroll) / FontSize); i++)
            {
                if (i >= 0 && i < Items.Count)
                {
                    string current = Items[i].ToString();
                    renderer.DrawText(current, Font, new SDLRectangle(0, i * FontSize - Scroll, CalculateWidth(current), FontSize), TextColor);
                }
            }
        }
    }
}

