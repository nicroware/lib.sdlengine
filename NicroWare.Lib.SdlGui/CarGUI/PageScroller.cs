﻿using NicroWare.Lib.SdlEngine;
using NicroWare.Lib.SdlEngine.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NicroWare.Lib.SdlGui
{
    public class PageScroller : GuiElement
    {
        public List<GuiPage> AllPages { get; private set; } = new List<GuiPage>();
        public GuiPage ActivePage { get; set; }
        int currentPage = 0;
        public static bool SideScroll = true;
        public int CurrentPage
        {
            get
            {
                return currentPage;
            }
            set
            {
                if (AllPages.Count == 0)
                    return;
                else if (value < 0)
                    currentPage = 0;
                else if (value >= AllPages.Count)
                    currentPage = AllPages.Count - 1;
                else
                    currentPage = value;
                ActivePage = AllPages[currentPage];
                ActivePage.Location = new SDLPoint(0, 0);
            }
        }
        MouseState beginDrag;

        public override void Update(GameTime gameTime)
        {
            PullEvents();
            if (last.IsKeyUp(MouseButtons.Left) && current.IsKeyDown(MouseButtons.Left))
                beginDrag = current;
            if (current.IsKeyDown(MouseButtons.Left))
            {
                if (Global.SideScroll && SideScroll)
                {
                    //TODO: Possible error if PageScroller is not at 0,0
                    if (current.Location.X > 6 && current.Location.X < Size.X - 6) // Just because if became funky and unstable in the sides
                        ActivePage.Location = new SDLPoint((current.Location.X - beginDrag.Location.X), 0);
                }
            }
            else if (current.IsKeyUp(MouseButtons.Left) && last.IsKeyDown(MouseButtons.Left))
            {
                if (Global.SideScroll && SideScroll)
                {
                    //Check if drag distance is long enough to do page switch
                    int dragDistance = current.Location.X - beginDrag.Location.X;
                    if (dragDistance < -100)
                        CurrentPage++;
                    else if (dragDistance > 100)
                        CurrentPage--;
                    else
                        ActivePage.Location = new SDLPoint(0, 0);
                }                
            }
            if (ActivePage == null && AllPages.Count > 0)
                CurrentPage = 0;
            if (ActivePage != null)
                ActivePage.Update(gameTime);
            base.Update(gameTime);
        }

        public void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            ActivePage.Draw(renderer, gameTime);
        }
    }
}
