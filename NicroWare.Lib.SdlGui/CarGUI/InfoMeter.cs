﻿using System;
using NicroWare.Lib.SdlEngine;

namespace NicroWare.Lib.SdlGui
{
    public class InfoMeter : CarGuiElement
    {
        public RendererFlip Flip { get; set; }

        SDLTexture containerTexture;
        SDLTexture valueTexture;

        private double displayValue;

        public InfoMeter()
        {
            Size = new SDLPoint(160, 180);
            Value = 0;
            MaxValue = 1;
            Flip = RendererFlip.None;
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            DrawContainer(renderer);
        }

        public void DrawContainer(SDLRenderer renderer)
        {
            DrawSurface surface = new DrawSurface(Size.X, Size.Y);
            DrawSurface valueSurf = new DrawSurface(Size.X, Size.Y);
            int defaultRad = (int)Math.Sqrt((150*150)+(225 * 225));
            for (int y = 0; y < surface.Height; y++)
            {
                for (int x = 0; x < surface.Width; x++)
                {
                    bool lines = y == 0 || x == surface.Width - 1;
                    int cirX = x + 150, cirY = y - 225;
                    int radius = (int)Math.Sqrt(cirX * cirX + cirY * cirY);
                    bool radEqual = radius == defaultRad;
                    bool radMore = radius >= defaultRad;
                    if (lines || radEqual || (y == surface.Height - 1 && radMore))
                    {
                        surface[x, y] = FrameColor;
                    }
                    else if (radMore)
                    {
                        surface[x, y] = new SDLColor(0, 0, 0, 0);
                    }
                    else
                        surface[x, y] = new SDLColor(255, 0, 0, 0);
                    valueSurf[x, y] = ValueColor;
                }
            }
            containerTexture = surface.MakeTexture(renderer);
            containerTexture.BlendMode = BlendMode.Blend;
            valueTexture = valueSurf.MakeTexture(renderer);
        }

        public override void Update(GameTime gameTime)
        {
            displayValue = Persent * this.Size.Y;
            base.Update(gameTime);
        }

        public override void Draw(Renderer renderer, GameTime gameTime)
        {
            //renderer.DrawTexture(valueTexture, new SDLRectangle(Location.X, Location.Y, containerTexture.Width, Value));
            if (Flip.HasFlag(RendererFlip.Vertical))
                renderer.Draw(valueTexture, new SDLRectangle(0, 0, containerTexture.Width, (int)displayValue));
            else 
                renderer.Draw(valueTexture, new SDLRectangle(0, (180 - (int)displayValue), containerTexture.Width, containerTexture.Height - (180 - (int)displayValue)));
            
            renderer.Draw(containerTexture,
                new SDLRectangle(0, 0, containerTexture.Width, containerTexture.Height),
                new SDLRectangle(0, 0, containerTexture.Width, containerTexture.Height),
                0, 
                new SDLPoint(0, 0),
                Flip);
        }
    }
}

