﻿using System;
using NicroWare.Lib.SdlEngine;

namespace NicroWare.Lib.SdlGui
{
    public static class MouseData
    {
        public static SDLPoint LastPos { get; set; }
        public static bool Down { get; set; }
        public static SDLPoint Pos { get; set; }
        public static SDLPoint LastDownPos { get; set; }

        /*public static int LastMouseX, LastMouseY;
        public static bool mouseDown = false;
        public static int lastMouseDownX = 0, lastMouseDownY = 0;
        public static int x, y;*/
    }
}

