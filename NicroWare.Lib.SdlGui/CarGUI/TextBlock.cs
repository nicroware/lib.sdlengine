﻿using System;
using NicroWare.Lib.SdlEngine;
using System.Collections.Generic;

namespace NicroWare.Lib.SdlGui
{
    public class TextBlock : GuiTextElement
    {
        public TextBlock()
        {
            
        }

        List<string> allLines;
        private void SplitText()
        {
            allLines = new List<string>();
            int charPerLine = (int)(Size.X / (FontSize * 0.6));
            int lineIndex = 0;
            string curLine = "";
            for(int j = 0; j < Text.Length; j++)
            {
                if (Text[j] == '\r')
                {
                    continue;
                }
                else if (Text[j] == '\n')
                {
                    allLines.Add(curLine);
                    curLine = "";
                    lineIndex = 0;
                    continue;
                }
                else if ((lineIndex > 0 && (lineIndex % charPerLine) == 0))
                {
                    allLines.Add(curLine);
                    curLine = "";
                }
                curLine += Text[j];
                lineIndex++;
            }
            allLines.Add(curLine);
        }



        public override void Draw(Renderer renderer, GameTime gameTime)
        {
            SplitText();
            int lineNumber = 0;
            foreach (string s in allLines)
            {
                if (s.Length > 0)
                {
                    int xStart = CalculateAlign(s);
                    renderer.DrawText(s, Font, new SDLRectangle(xStart, 5 + lineNumber * FontSize, (int)(FontSize * 0.6 * s.Length), FontSize), TextColor);
                }
                lineNumber++;
            }
        }
    }
}

