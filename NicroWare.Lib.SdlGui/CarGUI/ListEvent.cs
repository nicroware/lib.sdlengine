﻿using System;
using System.Collections.Generic;

namespace NicroWare.Lib.SdlGui
{
    public delegate void ListUpdate(object sender, EventArgs e);


    public class ListEvent<T> : List<T>
    {
        public event ListUpdate OnAdd;
        public ListEvent()
        {
        }

        public new void Add(T item)
        {
            base.Add(item);
            if (OnAdd != null)
                OnAdd(this, new EventArgs());
        }
    }
}

