﻿using System;
using NicroWare.Lib.SdlEngine;

namespace NicroWare.Lib.SdlGui
{
    public class ScrollBar : GuiValueElement
    {
        public SDLColor FrameColor { get; set; }
        public SDLColor BarColor { get; set; }
        public ScrollBar()
        {
            Size = new SDLPoint(75, 300);
            FrameColor = SDLColor.Blue;
            BarColor = SDLColor.Green;
        }

        SDLTexture container;
        SDLTexture cursor;
        bool CapturedMouse = false;

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            
            DrawSurface surf = new DrawSurface(Size.X, Size.Y);
            surf.CustomDraw((x, y) =>
            {
                if(x == 0 || y == 0 || x == Size.X - 1 || y == Size.Y - 1)
                    surf[x,y] = FrameColor;
            });
            container = surf.MakeTexture(renderer);

            DrawSurface drawCursor = new DrawSurface(Size.X, 25);
            drawCursor.CustomDraw((x, y) =>
            {
                drawCursor[x,y] = BarColor; 
            });
            cursor = drawCursor.MakeTexture(renderer);
        }

        public override void Update(GameTime gameTime)
        {
            if (MouseData.Down
                && Global.SideScroll 
                && MouseData.LastPos.X > Location.X 
                && MouseData.LastPos.X < Location.X + Size.X 
                && MouseData.LastPos.Y > Location.Y 
                && MouseData.LastPos.Y < Location.Y + Size.Y)
            {
                CapturedMouse = true;
                Global.SideScroll = false;
            }
            else if (MouseData.Down == false)
                CapturedMouse = false;
            if (CapturedMouse)
            {
                Value = (MaxValue - ((double)(MouseData.Pos.Y - Location.Y) / (double)Size.Y) * MaxValue);
            }
            
        }

        public override void Draw(NicroWare.Lib.SdlEngine.Renderer renderer, GameTime gameTime)
        {
            renderer.Draw(container, new SDLRectangle(0, 0, Size.X, Size.Y));
            renderer.Draw(cursor, new SDLRectangle(0, (int)(Size.Y - cursor.Height - (value / MaxValue * (Size.Y - cursor.Height))), cursor.Width, cursor.Height));
        }
    }
}

