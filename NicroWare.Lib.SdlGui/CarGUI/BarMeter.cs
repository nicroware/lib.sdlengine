﻿using System;
using NicroWare.Lib.SdlEngine;

namespace NicroWare.Lib.SdlGui
{
    public class BarMeter : CarGuiElement
    {
        public RendererFlip Flip { get; set; }

        SDLTexture containerTexture;
        SDLTexture valueTexture;

        private double displayValue;

        public BarMeter()
        {
            Size = new SDLPoint(50, 180);
            Value = 0;
            MaxValue = 1;
            Flip = RendererFlip.None;
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            DrawContainer(renderer);
        }

        public void DrawContainer(SDLRenderer renderer)
        {
            DrawSurface surface = new DrawSurface(Size.X, Size.Y);
            DrawSurface valueSurf = new DrawSurface(Size.X, Size.Y);
            for (int y = 0; y < surface.Height; y++)
            {
                for (int x = 0; x < surface.Width; x++)
                {
                    if (x == 0 || y == 0 || x == Size.X - 1 || y == Size.Y - 1)
                    {
                        surface[x, y] = FrameColor;
                    }
                    /*else
                        surface[x, y] = new SDLColor(255, 0, 0, 0);*/
                    valueSurf[x, y] = ValueColor;
                }
            }
            containerTexture = surface.MakeTexture(renderer);
            containerTexture.BlendMode = BlendMode.Blend;
            valueTexture = valueSurf.MakeTexture(renderer);
        }

        public override void Update(GameTime gameTime)
        {
            displayValue = Persent * this.Size.Y;
            base.Update(gameTime);
        }

        public override void Draw(Renderer renderer, GameTime gameTime)
        {
            //renderer.DrawTexture(valueTexture, new SDLRectangle(Location.X, Location.Y, containerTexture.Width, Value));
            if (Flip.HasFlag(RendererFlip.Vertical))
                renderer.Draw(valueTexture, new SDLRectangle(0, 0, containerTexture.Width, (int)displayValue));
            else 
                renderer.Draw(valueTexture, new SDLRectangle(0, (180 - (int)displayValue), containerTexture.Width, containerTexture.Height - (180 - (int)displayValue)));
            
            renderer.Draw(containerTexture,
                new SDLRectangle(0, 0, containerTexture.Width, containerTexture.Height),
                new SDLRectangle(0, 0, containerTexture.Width, containerTexture.Height),
                0, 
                new SDLPoint(0, 0),
                Flip);
        }
    }
}

