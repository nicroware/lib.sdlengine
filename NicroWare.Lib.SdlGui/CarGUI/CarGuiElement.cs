﻿using System;
using NicroWare.Lib.SdlEngine;

namespace NicroWare.Lib.SdlGui
{
    public class CarGuiElement : GuiValueElement
    {
        public SDLColor FrameColor { get; set; }
        public SDLColor ValueColor { get; set; }
        public CarGuiElement()
        {
            FrameColor = SDLColor.Red;
            ValueColor = SDLColor.Red;
        }
    }
}

